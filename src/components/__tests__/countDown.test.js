import { mount } from '@vue/test-utils'
import Countdown from '../Countdown.vue'

test('if component shows Dagen:Uren:Min:Sec', () => {
  const wrapper = mount(Countdown);
  expect(wrapper.text()).toBe('Coming Soon 0 Dagen : 0 Uren : 0 Min : 0 Sec')
});