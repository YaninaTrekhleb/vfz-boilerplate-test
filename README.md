# Countdown component on Vue for Vodafone-Ziggo test assignment. 
### (done by [Yanina Trekhleb](https://www.linkedin.com/in/yanina-trekhleb/))

![Demo](./src/images/countdown-demo.gif)

[Design Idea Origin](https://dribbble.com/shots/4796605-Daily-UI-014-Countdown-Timer)